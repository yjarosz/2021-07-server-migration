# Server migration FAQ


## What happened

The 22 July 2021 we migrated Gitlab infrastructure server to a newer location and a newer server.

The URL of the service has changed to https://gitlab.lcsb.uni.lu/. While is it fine to still use the old URL: http://git-r3lab.uni.lu/, we recommend you to change your bookmarks or remote to the new one.

This `FAQ` regroups questions or problems that already have been answered or found a solution. 

## Update your remote path.

Gitlab URL has changed to https://gitlab.lcsb.uni.lu/. While it is fine to not update anything as the old url will still works normally (https://git-r3lab.uni.lu/), you may want to update the remote of your git repositories with the new URL: Find some snippets [here](https://gitlab.lcsb.uni.lu/-/snippets/505).


## Error message when trying to do git operations

### Unexpexcted 404 or 500 error

This might happens if you are not logged in to https://gitlab.lcsb.uni.lu. Please Login to https://gitlab.lcsb.uni.lu. to resolve.


If you usually works from https://git-r3lab.uni.lu because it is your bookmarks, we advise you to change it to the new URL. Some methods in Gitlab server
will actually force you to use `gitlab.lcsb.uni.lu` DNS and it will not work if you are not logged in.  The login is usually tied to a DNS, so if you are logged in into git-r3lab.uni.lu, it doesn't mean that you are logged into gitlab.lcsb.uni.lu.


### DNS SPOOFING

```
WARNING: POSSIBLE DNS SPOOFING DETECTED! and WARNING: REMOTE HOST IDENTIFICATION HAS CHANGED!
```

As the server is not the same due to the migration, the key identifying the server is now different. This is a security mecanism to tell you that the server where you try to do git operations has changed and you have to pay attention of what is happening. In this case it is fine because the server as changed because of the migration process. 

You can supress this warning by running `ssh-keygen -R "[git-r3lab-server.uni.lu]:8022"`. It will supress the old identification key of the old server. 
And once you run again a git operation like `git pull` it will ask you to accept the new key identifier of the server. They key in question is `SHA256:9Pk5d+hAQwAPiYuDNLhCA8rS4fhTBb05TsQGWJVHvXk`.


### Docker

When trying to pull image from the local gitlab repository, you have to use the correct URL

```
ERROR: Preparation failed: failed to pull image "git-r3lab.uni.lu:4567...
```

change `git-r3lab.uni.lu` to `gitlab.lcsb.uni.lu`


## Another issue or question ?

Please contact us via our email: lcsb-syadmins@uni.lu.

